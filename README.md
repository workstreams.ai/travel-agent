# Travel Agent

**Demo project for the Voice & Travel Hackathon Berlin (27. & 28.09.2019)**\
Website: https://www.workstreams.ai/voice-travel-google-hackathon-berlin-september-27-28.html

**This project contains two versions:**
- **Basic Travel Agent** - Dialog Model without fulfillment
- **Advanced Travel Agent** - Dialog Model with fulfillment (using kiwi.com API)

## Basic Travel Agent
### Setup the Dialogflow agent
1. Login to [Dialogflow console](https://console.dialogflow.com/)
2. Create a new agent ([Google docs](https://cloud.google.com/dialogflow/docs/agents-manage#create))

![Create agent](./img/1_create_agent.png)


3. Restore **agents/Basic-Travel-Agent.zip** from this repo ([Google docs](https://cloud.google.com/dialogflow/docs/agents-settings#export))


![Restore agent](./img/2_restore_agent.png)

## Advanced Travel Agent with Express.js webserver and Kiwi API
### Prerequisites
- Node.js ^v8.3 (download at [nodejs.org](https://nodejs.org/en/download/))

### Tequila (kiwi.com API)
1. Create an account (as Person) at [Tequila](https://tequila.kiwi.com/portal/login/register)
2. Create an application
    - Type of partnership: Book with Kiwi.com
    - Product type: Search & Book

You can find the required **API key** in the details of the application ([My applications](https://tequila.kiwi.com/portal/my-applications))

### Install and start fulfillment
Navigate to the project directory on your locale machine and install all required node modules
```
npm install
```

Use the **API key** of your Tequila application to run the fulfillment. You can
you can also set your port of your webserver (default port is 8080)
```
API_KEY=[Your API key] PORT=8080 npm run start
```

### Setup the Dialogflow agent
1. Login to [Dialogflow](https://console.dialogflow.com)
2. Create a new agent ([Google docs](https://cloud.google.com/dialogflow/docs/agents-manage#create))
3. Restore **agents/Advanced-Travel-Agent.zip** ([Google docs](https://cloud.google.com/dialogflow/docs/agents-settings#export))

### Tunnel all fulfillment requests to your local machine
In the project root directory of your locale machine run the following command.
The number indicates the port you want to tunnel. Use the same port as your
webserver is currently running on.
```
./node_modules/.bin/ngrok http 8080
```
Use the ngork https URL as fulfillment webhook for your Dialogflow agent ([Google docs](https://cloud.google.com/dialogflow/docs/fulfillment-configure))
![webhook fulfillment](./img/3_webhook_fulfillment.png)

