const express = require('express');
const bodyParser = require('body-parser');
const { dialogflow } = require('actions-on-google');
const superagent = require('superagent');
const moment = require('moment');
const { get } = require('lodash');

const BASE_URL = 'https://kiwicom-prod.apigee.net';
const apiKey = process.env.API_KEY;

const app = express();
const port = process.env.PORT || 8080;

const dialogflowApp = dialogflow();

const _getCityCode = async (departureCity) => {
  const locationUrl = `${BASE_URL}/locations/query?term=${departureCity}`;
  const locationResponse = await superagent.get(locationUrl).set('apiKey', apiKey);

  return get(locationResponse, 'body.locations[0].code');
};

const _getFlightFromDepartureCity = async (code) => {
  const tomorrow = moment().add(1, 'days').format('DD/MM/YYYY');

  const flightUrl = `${BASE_URL}/v2/search?fly_from=city:${code}&date_from=${tomorrow}&date_to=${tomorrow}`;
  const flightResponse = await superagent.get(flightUrl).set('apiKey', apiKey);

  return get(flightResponse, 'body.data[0].cityTo');
};

dialogflowApp.intent('GetNextFlightIntent', async (conv, { departureCity }) => {
  if (!departureCity) {
    conv.ask(`What is the city of your departure?`);
    return
  }

  // Storing data in the conversation object persists only when the user is verified
  // conv.user.verification = 'VERIFIED'
  conv.user.storage.departureCity = departureCity;

  // Query kiwi.com API for the city ID
  const code = await _getCityCode(departureCity);
  console.log('CITY CODE - ', code);
  
  // Query kiwi.com API for flights with departure tomorrow from departureCity
  const destinationCity = await _getFlightFromDepartureCity(code);
  console.log('DESTINATION - ', destinationCity);
  
  // With SSML you can customize your prompts
  conv.ask(`<speak><prosody rate="slow" pitch="-2st">Hmm...</prosody>, the next flight from ${departureCity} goes to ${destinationCity}.</speak>`);
  conv.ask('Want to know what you can do until this flight starts?');
});

dialogflowApp.intent('GetNextFlightIntent - yes', (conv) => {
  conv.ask(`${conv.user.storage.departureCity} has some interesting museums.`);
  conv.close('But make sure you catch your flight. Bye');
});

app.use(bodyParser.json(), dialogflowApp).listen(port, () => console.log(`Example travel app listening on port ${port}`));
